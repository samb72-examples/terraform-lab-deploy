resource "azurerm_virtual_network" "vnet" {
  name                = "${var.region}-vnet"
  resource_group_name = "${azurerm_resource_group.lab_resource_group.name}"
  address_space       = ["${var.address_space}"]
  location            = "${var.region}"
}

resource "azurerm_subnet" "subnet" {
  name                 = "${var.region}-subnet"
  resource_group_name  = "${azurerm_resource_group.lab_resource_group.name}"
  virtual_network_name = "${azurerm_virtual_network.vnet.name}"
  address_prefix       = "${var.address_space}"
}

resource "azurerm_network_interface" "nic" {
  count               = "${var.lab_size}"
  name                = "${var.resource_group}-${count.index + 1}"
  location            = "${azurerm_resource_group.lab_resource_group.location}"
  resource_group_name = "${azurerm_resource_group.lab_resource_group.name}"

  ip_configuration {
    name                          = "testconfiguration1"
    subnet_id                     = "${azurerm_subnet.subnet.id}"
    private_ip_address_allocation = "Static"
    private_ip_address            = "10.0.0.${10 + count.index}"

    public_ip_address_id = "${element(azurerm_public_ip.public_ip.*.id, count.index)}"
  }
}

resource "azurerm_public_ip" "public_ip" {
  count                        = "${var.lab_size}"
  name                         = "lab-public-ip-${count.index + 1}"
  resource_group_name          = "${azurerm_resource_group.lab_resource_group.name}"
  location                     = "${var.region}"
  public_ip_address_allocation = "Dynamic"
}

data "azurerm_public_ip" "public_ip" {
  count               = "${var.lab_size}"
  name                = "lab-public-ip-${count.index + 1}"
  resource_group_name = "${azurerm_resource_group.lab_resource_group.name}"
  depends_on          = ["azurerm_virtual_machine.vm"]
}
