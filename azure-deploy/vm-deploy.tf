resource "azurerm_virtual_machine" "vm" {
  count                 = "${var.lab_size}"
  name                  = "${var.resource_group}-${count.index + 1}"
  location              = "${var.region}"
  resource_group_name   = "${azurerm_resource_group.lab_resource_group.name}"
  network_interface_ids = ["${element(azurerm_network_interface.nic.*.id, count.index)}"]
  vm_size               = "${var.vm_size}"

  delete_os_disk_on_termination    = true
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "${var.image_details["publisher"]}"
    offer     = "${var.image_details["offer"]}"
    sku       = "${var.image_details["sku"]}"
    version   = "${var.image_details["version"]}"
  }

  os_profile {
    computer_name  = "${var.resource_group}-${count.index + 1}"
    admin_username = "labadmin"
    admin_password = "${var.password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_os_disk {
    name              = "${var.resource_group}-disk-${count.index + 1}"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
}
