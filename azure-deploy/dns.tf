resource "azurerm_dns_a_record" "dns_record" {
  count               = "${var.lab_size}"
  name                = "${var.resource_group}-${count.index + 1}"
  zone_name           = "${var.dns_zone_details["zone"]}"
  resource_group_name = "${var.dns_zone_details["resource_group"]}"
  ttl                 = 300
  records             = [["${element(data.azurerm_public_ip.public_ip.*.ip_address, count.index)}"]]

  depends_on = ["azurerm_virtual_machine.vm"]
}
