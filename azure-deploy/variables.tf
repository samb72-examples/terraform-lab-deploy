variable "resource_group" {
  description = "Resource group to deploy components in to."
  default     = "lab-env"
}

variable "region" {
  description = "Region to deploy components in to."
  default     = "canadaeast"
}

variable "address_space" {
  default = "10.0.0.0/24"
}

variable "image_details" {
  description = "Image to deploy in lab."

  default = {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.3"
    version   = "7.3.20170925"
  }
}

variable "vm_size" {
  description = "Size of VMs to deploy."
  default     = "Standard_DS1_v2"
}

variable "lab_size" {
  description = "Number of machines to deploy to lab."
  default     = 3
}

variable "password" {
  description = "Password for lab VMs."
}

variable "dns_zone_details" {
  description = "DNS zone to create records for public DNS."

  default = {
    zone           = "labs.sjblab.com"
    resource_group = "static-infrastructure"
  }
}
